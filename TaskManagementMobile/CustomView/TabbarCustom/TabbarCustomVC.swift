//
//  TabbarCustomVC.swift
//  TaskManagementMobile
//
//  Created by Hùng on 28/04/2021.
//

import UIKit
import RAMAnimatedTabBarController
class TabbarCustomVC: RAMAnimatedTabBarController {
    
    //MARK: -Life cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
 
    
    //MARK: Func Configure
    func configure() {
        let tb1 = TaskChecklist()
        let tb2 = CreateNewTask()
        let tb3 = TaskDetails()
        let tb4 = UploadFile()
        let tb5 = UploadFileLoading()
        
        let nav1 = UINavigationController(rootViewController: tb1)
        tb1.tabBarItem = RAMAnimatedTabBarItem(title: "", image: UIImage(named: "calendar"), tag: 1)
        (tb1.tabBarItem as? RAMAnimatedTabBarItem)?.animation = RAMFlipTopTransitionItemAnimations()
        
        let nav2 = UINavigationController(rootViewController: tb2)
        tb2.tabBarItem = RAMAnimatedTabBarItem(title: "", image: UIImage(named: "activity"), tag: 1)
        (tb2.tabBarItem as? RAMAnimatedTabBarItem)?.animation = RAMFlipTopTransitionItemAnimations()
        
        let nav3 = UINavigationController(rootViewController: tb3)
        tb3.tabBarItem = RAMAnimatedTabBarItem(title: "", image: UIImage(named: "plus"), tag: 1)
        (tb3.tabBarItem as? RAMAnimatedTabBarItem)?.animation = RAMBounceAnimation()
        
        let nav4 = UINavigationController(rootViewController: tb4)
        tb4.tabBarItem = RAMAnimatedTabBarItem(title: "", image: UIImage(named: "document"), tag: 1)
        (tb4.tabBarItem as? RAMAnimatedTabBarItem)?.animation = RAMBounceAnimation()
        
        let nav5 = UINavigationController(rootViewController: tb5)
        tb5.tabBarItem = RAMAnimatedTabBarItem(title: "", image: UIImage(named: "setting"), tag: 1)
        (tb5.tabBarItem as? RAMAnimatedTabBarItem)?.animation = RAMBounceAnimation()
        
        setViewControllers([nav1, nav2, nav3, nav4, nav5], animated: false)
    }
    
    
}
