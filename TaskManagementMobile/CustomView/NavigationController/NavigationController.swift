//
//  NavigationCustom.swift
//  TaskManagementMobile
//
//  Created by Hùng on 28/04/2021.
//

import Foundation
import UIKit

final class NavigationController: UINavigationController {
    
   
   //MARK: -Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }

    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        configureNavigation()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if let preferredStatusBarStyle = topViewController?.preferredStatusBarStyle {
            return preferredStatusBarStyle
        }
        return .lightContent
    }
    
    func configureNavigation() {
        self.navigationBar.isTranslucent = false
        self.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),
            NSAttributedString.Key.font: UIFont.init(name: "Avenir-Medium", size: 17) ?? UIFont()
        ]
    }
 
}
//MARK: -Extension
extension NavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        let backBarButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backBarButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        viewController.navigationItem.backBarButtonItem = backBarButton
    }
}

