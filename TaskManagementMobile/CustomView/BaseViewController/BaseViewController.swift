//
//  BaseVC.swift
//  TaskManagementMobile
//
//  Created by Hùng on 28/04/2021.
//
import UIKit
import Foundation

class BaseViewController: UIViewController {
    //MARK: -LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        configureNaviButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
                self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    //MARK: -Func Custom
    func configureNaviButton() {
        self.addRightButton(firstImage: UIImage(named: "notification")!, secondImage: UIImage(named: "heart")!)
        self.addLeftButtonNavi( UIImage(named: "search")!)
    }
    
    func addLeftButtonNavi( _ leftImageButton: UIImage) {
        let leftButton = barButton(image: leftImageButton , selector: #selector(leftButtonAction))
        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    func checkNotifi() {
        addRightButton(firstImage: UIImage(named: "search")! , secondImage:  UIImage(named: "notification")!)
    }
    
    func setBackgroundNaviBar(color: UIColor) {
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
    func addRightButton(firstImage: UIImage, secondImage: UIImage) {
        let firstButton = barButton(image: firstImage , selector: #selector(firstButtonAction))
        let secondButton = barButton(image: secondImage , selector: #selector(secondButtonAction))
        navigationItem.rightBarButtonItems  = [firstButton, secondButton]
        
    }
    func addRightButtonCustom(secondImage: UIImage) {
        let customview = CustomBadgeNaviButton(frame: CGRect(x: 0, y: 0, width: 20 , height: 20))
        //    customview.delegate = self
        //    customview.lblTitleBadge.text = "2"
        let secondButton = barButton(image: secondImage , selector: #selector(secondButtonAction))
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: customview), secondButton]
    }
    
    private func imageView(image: UIImage) -> UIImageView {
        let logo = image
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = logo
        logoImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        return logoImageView
    }
    
    func addLeftTitleNavi(title: String) {
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.textColor = .white
        titleLabel.font = UIFont.init(name: "SFProText-Semibold", size: 17)
        titleLabel.sizeToFit()
        let leftItem = UIBarButtonItem(customView: titleLabel)
        self.navigationItem.leftBarButtonItem = leftItem
    }
    
    @objc func firstButtonAction() {
        print("action firt button")
    }
    
    @objc func secondButtonAction() {
        print("action second button")
    }
    @objc func leftButtonAction() {
        print("action leftButtonAction")
    }
    
    
    func setTitleNavi(title: String, titleColor: UIColor) {
        let titleNavi = UILabel()
        titleNavi.text = title
        titleNavi.font = UIFont.init(name: "SFProText-Semibold", size: 17)
        titleNavi.textColor = titleColor
        navigationItem.titleView = titleNavi
        
    }
    func setTitleNaviCustom(title: String) {
        let naviView = CustomNaviView(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
        //        naviView.lblTitleNavi.text = title
        //        naviView.imgNavi.image = UIImage(named: "artboard1")
        navigationItem.titleView = naviView
    }
    
    func barImageView(image: UIImage) -> UIBarButtonItem {
        return UIBarButtonItem(customView: imageView(image: image))
    }
    
    func barButton(image: UIImage, selector: Selector) -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        button.widthAnchor.constraint(equalToConstant: 35).isActive = true
        button.heightAnchor.constraint(equalToConstant: 35).isActive = true
        button.addTarget(self, action: selector, for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }
    
    func customTitle(title: String?) {
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 335, height:  view.frame.height))
        titleLabel.text = title ?? ""
        titleLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        titleLabel.font = UIFont.init(name: "Avenir-Medium", size: 17)
        navigationItem.titleView = titleLabel
        navigationController?.navigationBar.backItem?.title = ""
    }
}
//MARK: -Extension
extension BaseViewController: BadgeNaviDelegate {
    func actionSelectShow(_ cell: CustomBadgeNaviButton) {
        print("check")
    }
}
