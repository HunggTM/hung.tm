//
//  TaskChecklist.swift
//  TaskManagementMobile
//
//  Created by Hùng on 27/04/2021.
//

import UIKit

class TaskChecklist: BaseViewController {
    
    //MARK: - Menu
    var listItems = [    MenuItem(content: "UX Research", detail: "Branding", isSeleced: false),                                               MenuItem(content: "Awareness Research", detail: "UI/UX", isSeleced: false),
                         MenuItem(content: "Create New UI - Pertamina", detail: "UI/UX", isSeleced: false),
                         MenuItem(content: "Moodboard Audience", detail: "Digital", isSeleced: false),
                         MenuItem(content: "Audience Moodboard", detail: "Strategist", isSeleced: false),
                         MenuItem(content: "Color Pallete", detail: "Digital", isSeleced: false),
                         MenuItem(content: "Tipography Study", detail: "Strategist", isSeleced: false),
                         MenuItem(content: "Sketch basic Logo", detail: "Branding", isSeleced: false),
                         MenuItem(content: "Purchase Mockup", detail: "Digital", isSeleced: false)]
    //MARK: - IBOutlet
    @IBOutlet weak var tableViewTask: UITableView!
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Task"
        configureNaviButton()
        configureTableView()
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    //MARK: -ConfigureTableView
    func configureTableView() {
        tableViewTask.delegate = self
        tableViewTask.dataSource = self
        tableViewTask.register(UINib(nibName: "TaskChecklistCell", bundle: nil), forCellReuseIdentifier: "TaskChecklistCell")
    }
}

//MARK: - Extension
extension TaskChecklist: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewTask.dequeueReusableCell(withIdentifier: "TaskChecklistCell") as! TaskChecklistCell
        let item = listItems[indexPath.row]
        cell.set(item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for index in 0...self.listItems.count - 1 {
            if index == indexPath.row {
                self.listItems[index].isSeleced = true
            } else {
                self.listItems[index].isSeleced = false
            }
        }
        tableViewTask.reloadData()
    }
}
