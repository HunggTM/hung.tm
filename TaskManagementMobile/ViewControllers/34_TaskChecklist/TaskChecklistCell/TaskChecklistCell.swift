//
//  TaskChecklistCell.swift
//  TaskManagementMobile
//
//  Created by Hùng on 29/04/2021.
//

import UIKit

class TaskChecklistCell: UITableViewCell {
    
    @IBOutlet weak var backgroundViewCell: UIView!
    @IBOutlet weak var checkboxImage: UIImageView!
    @IBOutlet weak var titleCell: UILabel!
    @IBOutlet weak var styleView: UIView!
    @IBOutlet weak var titleStyleCell: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code. #72809D.     #702DE3
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func set(_ item: MenuItem) {
        backgroundViewCell.layer.cornerRadius = 10
        styleView.layer.cornerRadius = 10
        styleView.backgroundColor = UIColor.green
        titleCell.text = item.content
        titleStyleCell.text = item.detail
        self.configureUIWithStatusSeleced(item.isSeleced)
        
        
    }
    private func configureUIWithStatusSeleced(_ isSelected: Bool) {
        backgroundViewCell.backgroundColor = ( isSelected == false ) ? UIColor(red: 211/255, green: 213/255, blue: 215/255, alpha: 1.0) : UIColor.white
        backgroundViewCell.layer.borderWidth = (isSelected == true) ? 1 : 0
        backgroundViewCell.layer.borderColor =  (isSelected == true) ? UIColor.purple.cgColor : UIColor.clear.cgColor
        imageView?.image = (isSelected == true) ? UIImage(named: "checkbox") : UIImage(named: "check")
        
    }
    
}
