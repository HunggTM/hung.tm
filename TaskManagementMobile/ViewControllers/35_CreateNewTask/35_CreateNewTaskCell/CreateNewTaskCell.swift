//
//  35_CreateNewTaskCell.swift
//  TaskManagementMobile
//
//  Created by Hùng on 04/05/2021.
//

import UIKit

class CreateNewTaskCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelConten: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    func set(_ item: ListTask) {
        view.layer.cornerRadius = 10
        labelConten.text = item.content
        self.cofigureStatus(item.isSeleced)
    }
    
    private func cofigureStatus(_ isSeleced: Bool) {
        view.backgroundColor = (isSelected == false ) ? UIColor(red: 211/255, green: 213/255, blue: 215/255, alpha: 1.0) : UIColor.white
        view.layer.borderWidth = (isSeleced == true) ? 1 : 0
        view.layer.borderColor = (isSeleced == true) ? UIColor.purple.cgColor : UIColor.clear.cgColor
        imageView?.image = (isSeleced == true) ? UIImage(named: "pl") : UIImage(named: "check")
    }
}
