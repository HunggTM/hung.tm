//
//  CreateNewTask.swift
//  TaskManagementMobile
//
//  Created by Hùng on 27/04/2021.
//

import UIKit

class CreateNewTask: BaseViewController {
    
    //MARK: - Menu
    var listTask = [ListTask(content: "Color Pallete", isSeleced: false),
                    ListTask(content: "Tipography Study", isSeleced: false),
                    ListTask(content: "Purchase Mockup", isSeleced: false),
                    ListTask(content: "Moodboard Research for Color", isSeleced: false)]
    
    //MARK: - IBOutlet
    @IBOutlet weak var tableViewNewTask: UITableView!
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Create"
        configureNaviButton()
        configureTableViewNewTask()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: - IBAction
    @IBAction func clickOK(_ sender: Any) {
    }
    
    //MARK: - Configure
    func configureTableViewNewTask() {
        tableViewNewTask.delegate = self
        tableViewNewTask.dataSource = self
        tableViewNewTask.register(UINib(nibName: "CreateNewTaskCell", bundle: nil), forCellReuseIdentifier: "CreateNewTaskCell")
    }
}
//MARk: - Extesion
extension CreateNewTask: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listTask.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewNewTask.dequeueReusableCell(withIdentifier: "CreateNewTaskCell") as? CreateNewTaskCell
        let item = listTask[indexPath.row]
//        cell.set(item)
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for index in 0...self.listTask.count - 1 {
            if index == indexPath.row {
                self.listTask[index].isSeleced = true
            } else {
                self.listTask[index].isSeleced = false
            }
        }
        tableViewNewTask.reloadData()
    }

}
