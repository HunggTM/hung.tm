//
//  Splash.swift
//  TaskManagementMobile
//
//  Created by Hùng on 27/04/2021.
//

import UIKit

class Splash: BaseViewController {
    
    //MARK - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNaviButton()
    }
    
    //MARK - IBAction
    @IBAction func clickOK(_ sender: Any) {
        AppDelegate.shaped.initTabbar()
    }
  
}
