//
//  UploadFile.swift
//  TaskManagementMobile
//
//  Created by Hùng on 27/04/2021.
//

import UIKit

class UploadFile: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Upload"
        configureNaviButton()
    }
}
