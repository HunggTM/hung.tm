//
//  Extension.swift
//  TaskManagementMobile
//
//  Created by Hùng on 27/04/2021.
//

import Foundation
import UIKit
extension String {
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    @IBInspectable
    var topCornerRadiusLeft: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            roundCorners(corners: [.topLeft], radius: newValue)
        }
    }
    
    @IBInspectable
    var topCornerRadiusRight: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            roundCorners(corners: [.topRight], radius: newValue)
        }
    }
    @IBInspectable
    var bottomCornerRadiusLeft: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            roundCorners(corners: [.bottomLeft], radius: newValue)
        }
    }
    
    @IBInspectable
    var bottomCornerRadiusRight: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            roundCorners(corners: [.bottomRight], radius: newValue)
        }
    }
    @IBInspectable
    var cornerRadius : CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        
        get {
            return layer.cornerRadius
        }
    }
    @IBInspectable
    var borderColor : CGColor? {
        set {
            layer.borderColor =  newValue
        }
        
        get {
            return layer.borderColor
        }
    }
    @IBInspectable
    var borderWidth : CGFloat {
        set {
            layer.borderWidth = newValue
        }
        
        get {
            return layer.borderWidth
        }
    }
    public var width: CGFloat {
        return self.frame.size.width
    }
    public var height: CGFloat {
        return self.frame.size.height
    }
    public var top: CGFloat {
        return self.frame.origin.y
    }
    public var bottom: CGFloat {
        return self.frame.height + self.frame.origin.y
    }
    public var left: CGFloat {
        return self.frame.origin.x
    }
    public var right: CGFloat {
        return self.frame.size.width + self.frame.origin.x
    }
}

extension Notification.Name{
    static let didLoginNotification = Notification.Name("didLoginNotification")
}

