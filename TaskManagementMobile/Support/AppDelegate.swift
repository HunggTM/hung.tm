//
//  AppDelegate.swift
//  TaskManagementMobile
//
//  Created by Hùng on 27/04/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

var window: UIWindow?
    static let shaped: AppDelegate = {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            
            fatalError("Cannot cast the AppDelegate")
        }
        return delegate
    }()

    var naviController: NavigationController?
    lazy var tabbarController = TabbarCustomVC()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        window?.makeKeyAndVisible()
        setRootView(centerVC: Splash())
        return true
    }
    func setRootView(centerVC: UIViewController) {
//        let tabbar = TabbarCustomVC()
//        self.tabbarController = tabbar
//        let tab = TabbarCustomVC()
        let tab = Splash()
        let navi = NavigationController(rootViewController: tab)
        self.naviController = navi
        self.window?.rootViewController = navi
    }
    
    func initTabbar() {
        let tabbar = TabbarCustomVC()
        self.window?.rootViewController = tabbar
    }
}
